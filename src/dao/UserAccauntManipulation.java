package dao;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import model.UserAccount;
import my_exception.TransferException;
import utils.FileOperationUtils;

public class UserAccauntManipulation {
  Logger log = Logger.getLogger(UserAccauntManipulation.class);
  final static String PATH = "src/userAccounts";
  UserAccount sender;
  UserAccount recipient;

  public void transferToOneUsers(List<Integer> idRecipientList, int idSender, Long transfer)
      throws TransferException {
    ExecutorService service = Executors.newFixedThreadPool(5);
    idRecipientList.stream().forEach(c -> {
      service.submit(new Runnable() {
        public void run() {
          try {
            transfer(idSender, c, transfer);
          } catch (TransferException e) {
            e.printStackTrace();
          }
        }
      });
    });
  }

  public void transferToMoreUsers(int idSender, List<Integer> idRecipientList, Long transfer)
      throws TransferException {
    ExecutorService service = Executors.newFixedThreadPool(5);
    idRecipientList.stream().forEach(c -> {
      service.submit(new Runnable() {
        public void run() {
          try {
            transfer(idSender, c, transfer);
          } catch (TransferException e) {
            e.printStackTrace();
          }
        }
      });
    });
  }

  public synchronized void transfer(int idSender, int idRecipient, Long transfer)
      throws TransferException {
    sender = null;
    recipient = null;
    FileOperationUtils fou = new FileOperationUtils();
    List<File> userAccountDirFile = fou.listFilesWithSubFolders(new File(PATH));
    userAccountDirFile.stream().forEach(c -> {
      InputStream fis = null;
      ObjectInputStream oin = null;
      try {
        UserAccount user = null;
        fis = new FileInputStream(c);
        InputStream is = new BufferedInputStream(fis);
        oin = new ObjectInputStream(is);
        user = (UserAccount) oin.readObject();
        if (user.userID == idSender) {
          sender = user;
        }
        if (user.userID == idRecipient) {
          recipient = user;
        }
      } catch (EOFException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (oin != null) {
            oin.close();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    if (sender.balance > 0 && recipient != null) {
      sender.balance = sender.balance - transfer;
      log.info(sender.balance + ":" + idSender);
      recipient.balance = recipient.balance + transfer;
      log.info(recipient.balance + ":" + idRecipient);
    } else if (sender.balance <= 0) {
      throw new TransferException("Balance zero");
    } else if (recipient == null) {
      throw new TransferException("Not recipient");
    }
    List<UserAccount> result = new ArrayList<UserAccount>();
    result.add(sender);
    result.add(recipient);
    createSerializFileAccaunt(result);
  }

  public void createSerializFileAccaunt(List<UserAccount> accountList) {
    for (UserAccount user : accountList) {
      File file = new File(PATH + "\\" + user.userID);
      file.getParentFile().mkdirs();
      try {
        file.createNewFile();
        log.info("Create file " + user.userID);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos;
        oos = new ObjectOutputStream(fos);
        oos.writeObject(user);
        oos.flush();
        oos.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
