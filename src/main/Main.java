package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import dao.UserAccauntManipulation;
import model.UserAccount;
import my_exception.TransferException;

public class Main {

  public static void main(String[] args) {
    List<UserAccount> accountList = new ArrayList<UserAccount>();
    accountList.add(new UserAccount(337599, "����", Long.parseLong("3333")));
    accountList.add(new UserAccount(337222, "�������", Long.parseLong("3333")));
    accountList.add(new UserAccount(337345, "������", Long.parseLong("3333")));
    accountList.add(new UserAccount(337678, "�����", Long.parseLong("3333")));
    accountList.add(new UserAccount(337122, "��������", Long.parseLong("3333")));
    accountList.add(new UserAccount(337356, "�����", Long.parseLong("3333")));
    UserAccauntManipulation uam = new UserAccauntManipulation();
    uam.createSerializFileAccaunt(accountList);
    try {
      uam.transfer(337599, 337122, Long.parseLong("333"));
    } catch (NumberFormatException e) {
      e.printStackTrace();
    } catch (TransferException e) {
      e.printStackTrace();
    }
    try {
      uam.transferToMoreUsers(337599, Arrays.asList(337222, 337345, 337678, 337122, 337356),
          Long.parseLong("3"));
    } catch (NumberFormatException | TransferException e) {
      e.printStackTrace();
    }
    try {
      uam.transferToOneUsers(Arrays.asList(337222, 337345, 337678, 337122, 337356), 337599,
          Long.parseLong("3"));
    } catch (NumberFormatException | TransferException e) {
      e.printStackTrace();
    }
    for(int i = 0; i<1001; i++){
      try {
        uam.transferToOneUsers(Arrays.asList(337222, 337345, 337678, 337122, 337356), 337599,
            Long.parseLong("3"));
      } catch (NumberFormatException e) {
        e.printStackTrace();
      } catch (TransferException e) {
        e.printStackTrace();
      }
    }
  }

}
