package utils;

import java.io.File;
import java.util.ArrayList;

public class FileOperationUtils {
  public ArrayList<File> listFilesWithSubFolders(File dir) {
    ArrayList<File> files = new ArrayList<File>();
    for (File file : dir.listFiles()) {
        if (file.isDirectory())
            files.addAll(listFilesWithSubFolders(file));
        else
            files.add(file);
    }
    return files;
}
}
