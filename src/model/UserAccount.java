package model;

import java.io.Serializable;

public class UserAccount implements Serializable {
  private static final long serialVersionUID = -5897451648988668211L;
  public int userID;
  public String userName;
  public Long balance;
  
  public UserAccount(int userID, String userName, Long balance) {
    super();
    this.userID = userID;
    this.userName = userName;
    this.balance = balance;
  }

}
